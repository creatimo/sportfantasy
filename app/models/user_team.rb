class UserTeam < ActiveRecord::Base
  belongs_to :user
  belongs_to :league
  has_many :user_player_deals, :dependent => :destroy
  has_many :players, :through => :user_player_deals

  enum status: %i[draft ready active finished archived]

  scope :draft, -> { where(status: statuses[:draft]) }
  scope :ready, -> { where(status: statuses[:ready]) }
  scope :active, -> { where(status: statuses[:active]) }
end
