class Game < ActiveRecord::Base
  belongs_to :league
  belongs_to :venue
  belongs_to :parser
  has_many :team_participations
  has_many :team, :through => :team_participations

  enum status: [
    :upcoming,
    :finished,
    :parsed,
    :processed,
    :archived
  ]

  enum parser: [
    :hokejbal_cz,
    :onlajny_com
  ]

  scope :in_round, -> {
    where("term between ? and ?",
          ApplicationController.helpers.prev_update,
          ApplicationController.helpers.next_update)
  }
end
