class Team < ActiveRecord::Base
  has_many :players
  belongs_to :league
  has_many :team_participations
  has_many :games, :through => :team_participations
end
