class League < ActiveRecord::Base
  has_many :teams
  has_many :user_teams
  belongs_to :sport
end
