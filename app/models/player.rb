class Player < ActiveRecord::Base
  belongs_to :team
  belongs_to :player_role
  has_many :user_player_deals
  has_many :user_teams, :through => :user_player_deals
end
