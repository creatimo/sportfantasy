class Log < ActiveRecord::Base
  belongs_to :user

  enum event: [
    :user_team_create,
    :user_team_player_assign,
    :user_score_update_player
  ]

end
