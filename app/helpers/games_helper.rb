module GamesHelper

  def next_update
    today = Date.today
    (today + (Settings.update_day - today.cwday) % 7).midnight # saturday
  end

  def prev_update
    # next_update - 7.days
    next_update - 1.month
  end
end
