module LogsHelper
  def create_log (user, event, key, value)
    Log.create(user_id: user, event: event, event_key: key, event_value: value)
  end
end
