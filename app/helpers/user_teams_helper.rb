module UserTeamsHelper
  def user_add_check (player, team)
    sportName = team.league.sport.name
    sportNameCode = sportName.parameterize.underscore
    allow_players = Settings.sports[sportNameCode].rules.game.players.to_i
    check_result = { status: :success }

    # check count of players
    if user_team_players_count(team) >= allow_players
      check_result = { status: :error, notice: t('.too_much_players') }

    # check if already assigned
    elsif user_team_players(team).any? {|u| u.id == player.id}
      check_result = { status: :error, notice: t('.player_already_assigned') }

    # check count of players from same team
    elsif same_team_players_count(team, player) >= 3
      check_result = { status: :error, notice: t('.too_much_players_from_same_team') }
    end

    return check_result
  end

  def same_team_players_count (team, player)
    user_team_players(team).select{|p| p.team.id == player.team.id}.count.to_i
  end

  def user_team_players (team)
    team.players
  end

  def user_team_players_count (team)
    user_team_players(team).count.to_i
  end
end
