runBarSliders = ->
  $scoreBar = $(".score-bar")
  scoreBarCount = $scoreBar.find('li').length
  $scoreBar.bxSlider
    minSlides: 1
    maxSlides: 10
    slideWidth: 120
    slideMargin: 10
    ticker: true
    speed: 3800*scoreBarCount

  $featureBar = $(".feature-bar")
  featureBarCount = $featureBar.find('li').length
  $featureBar.bxSlider
    minSlides: 1
    maxSlides: 10
    slideWidth: 170
    slideMargin: 10
    ticker: true
    speed: 3000*featureBarCount

$(document).ready(runBarSliders)
$(document).on('page:load', runBarSliders)
