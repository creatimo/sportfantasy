onReadyFn = ->
  next_friday_midnight = moment().day(6).startOf('day').toDate()
  $('.countdown').countdown({ until: new Date(next_friday_midnight) })

  $('.logs li').each ->
    $this = $(this)
    date = $this.attr 'data-created'
    $this.find('.ctd').text moment(date, "X").fromNow()


$(document).ready(onReadyFn)
$(document).on('page:load', onReadyFn)
