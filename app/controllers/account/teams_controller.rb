class Account::TeamsController < ApplicationController
  before_action :set_team, only: [:show, :update, :add_player, :destroy, :set_ready, :set_draft, :set_captain]
  before_filter :authenticate_user!
  include UserTeamsHelper

  def index
    @teams = UserTeam.where(user_id: current_user.id)
  end

  def new
    @team = UserTeam.new
  end

  def show
    if !@UserTeam.user_id == current_user.id
      redirect_to account_dashboard_path, notice: 'K tomuto týmu nemáte dostatečná práva'
    end
  end

  def edit
    @team = UserTeam.find(params[:id])
  end

  def update
    if @UserTeam.update_attributes(user_team_params)
      redirect_to @UserTeam, :notice => "Team updated."
    else
      redirect_to @UserTeam, :alert => "Unable to update team."
    end
  end

  def create
    @team = UserTeam.new(user_team_params)

    @team.user_id = current_user.id

    respond_to do |format|
      if @team.save
        create_log(current_user.id , :user_team_create, @team.id, 0)
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @UserTeam.destroy
    respond_to do |format|
      format.html { redirect_to account_teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_player
    deal = UserPlayerDeal.new
    player = Player.find(params[:player])
    deal.player_id = player.id
    deal.user_team_id = @UserTeam.id

    check_status = user_add_check(player, @UserTeam)

    if check_status[:status] == :success
      if deal.save
        respond_to do |format|
          create_log(current_user.id , :user_team_player_assign, deal.player_id, deal.user_team_id)
          format.html { redirect_to account_team_path(params[:id]) }
          format.js {
            render :json => {
              status: :success,
              :html => ( render_to_string 'account/teams/_player_item', :locals => { player: player } )
            }
          }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to account_team_path(params[:id]), notice: check_status[:notice] }
        format.js {
          render :json => {
            status: check_status[:status],
            notice: check_status[:notice]
          }
        }
      end
    end
  end

  def remove_player
    respond_to do |format|
      if UserPlayerDeal.destroy_all(player_id: params[:player], user_team_id: params[:id])
        format.js
        format.html { redirect_to account_team_path(params[:id]) }
        format.json { render json: {status: :true} }
      else
        format.js
        format.html { redirect_to account_team_path(params[:id]), notice: 'nene' }
        format.json { render json: 'chyba' }
      end
    end
  end

  def set_ready
    set_status(:ready)
  end

  def set_draft
    set_status(:draft)
  end

  def set_captain
    UserPlayerDeal.where(user_team_id: params[:id]).update_all(:captain => false)

    player_deal = UserPlayerDeal.find_by(player_id: params[:player], user_team_id: params[:id])
    player_deal.captain = true
    if player_deal.save
      redirect_to @UserTeam
    else
      raise 'Čas na paniku'
    end
  end

  private

    def user_team_params
      params.require(:user_team).permit(:name, :league_id)
    end

    def set_team
      @UserTeam = UserTeam.find(params[:id])
    end

    def set_status(status)
      @UserTeam.status = status
      if @UserTeam.save
        redirect_to @UserTeam
      else
        raise 'Čas na paniku'
      end
    end
end
