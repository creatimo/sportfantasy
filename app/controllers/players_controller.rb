class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :detail_team_manage ,:edit, :update, :destroy]

  def index
    @players = Player.all.page params[:page]
  end

  def show
    respond_to do |format|
      format.html { render :show }
      format.json { render json: @player }
    end
  end

  def detail_team_manage
    team = Team.find(params[:team_id])
    respond_to do |format|
      format.json {
        render :json => {
          :success => true,
          :html => ( render_to_string 'players/_detail_team_manage', :locals => { player: @player, team: team } )
        }
      }
    end
  end

  private

    def set_player
      @player = Player.find(params[:id])
    end
end
