class AccountController < ApplicationController
  before_filter :authenticate_user!

  def dashboard
    @logs = Log.where(user_id: current_user.id).order('id DESC').limit(30)
  end

  def teams
  end
end
