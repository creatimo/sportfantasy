class ManagementController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only

  def index
    @games_in_round = Game.in_round
  end

  def set_ready_teams_active
    update = UserTeam.ready.update_all(status: UserTeam.statuses[:active])
    redirect_to management_path(), notice: "Teams actived: #{update.to_i}"
  end

  def set_active_teams_draft
    update = UserTeam.active.update_all(status: UserTeam.statuses[:draft])
    redirect_to management_path(), notice: "Teams drafted: #{update.to_i}"
  end

  def parse_results
    game_for_parse = Game.first
    Parser.parse(game_for_parse)
  end

  def process_results
    raise 'run rake results:process'
  end

  private

  def admin_only
    unless current_user.admin?
      redirect_to :back, :alert => "Access denied."
    end
  end
end
