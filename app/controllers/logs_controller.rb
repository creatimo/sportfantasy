class LogsController < ApplicationController
  def create
    @team = UserTeam.new(user_team_params)

    @team.user_id = current_user.id

    respond_to do |format|
      if @team.save
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    def user_team_params
      params.require(:user_team).permit(:name, :league_id)
    end
end
