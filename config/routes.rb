Rails.application.routes.draw do
  get 'players/index'

  get 'team/index'

  get 'account/dashboard'

  namespace :account do
    resources :dashboard, :teams
    put 'teams/:id/set_draft', to: 'teams#set_draft', as: 'set_draft'
    put 'teams/:id/set_ready', to: 'teams#set_ready', as: 'set_ready'
    put 'teams/:id/set_captain', to: 'teams#set_captain', as: 'set_captain'
    post '/teams/:id/add_player', to: 'teams#add_player', as: 'add_team_player'
    delete '/teams/:id/remove_player', to: 'teams#remove_player', as: 'remove_player'
  end

  get 'management', to: 'management#index'
  get 'parse_results', to: 'management#parse_results', as: 'parse_results'
  get 'process_results', to: 'management#process_results', as: 'process_results'
  get 'make_ready_teams_active', to: 'management#make_ready_teams_active', as: 'make_ready_teams_active'
  get 'make_active_teams_draft', to: 'management#make_active_teams_draft', as: 'make_active_teams_draft'

  resources :user_teams, path: '/account/teams'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Upmin::Engine => '/admin'
  root to: 'static_page#homepage'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  resources :teams
  resources :players

  get '/players/:id/detail_team_manage', to: 'players#detail_team_manage', as: 'player_detail_team_manage'

  resources :user_player_deals
end
