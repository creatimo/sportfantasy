class AddCaptainToUserPlayerDeals < ActiveRecord::Migration
  def change
    add_column :user_player_deals, :captain, :boolean, default: false
  end
end
