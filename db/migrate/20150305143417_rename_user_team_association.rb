class RenameUserTeamAssociation < ActiveRecord::Migration
  def change
    remove_column :user_player_deals, :userteam_id
    add_column :user_player_deals, :user_team_id, :integer
  end
end
