class DefaultValuUserTeams < ActiveRecord::Migration
  def change
    change_column :user_teams, :status, :integer, :default => 0
  end
end
