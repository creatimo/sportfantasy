class RemoveGameResultsTable < ActiveRecord::Migration
  def change
    drop_table :game_results
  end
end
