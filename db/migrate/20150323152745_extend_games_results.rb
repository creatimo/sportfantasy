class ExtendGamesResults < ActiveRecord::Migration
  def change
    add_column :games, :venue_id, :integer
    add_column :games, :results_source, :string
    add_column :games, :parser_id, :integer
    add_column :games, :parsed_results, :text
    add_column :games, :status, :integer
  end
end
