class ChangeParserColumn < ActiveRecord::Migration
  def change
    remove_column :games, :parser_id
    add_column :games, :parser, :integer
  end
end
