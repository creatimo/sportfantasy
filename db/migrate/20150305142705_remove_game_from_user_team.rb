class RemoveGameFromUserTeam < ActiveRecord::Migration
  def change
    remove_column :user_teams, :game_id
  end
end
