class CreateLogEvents < ActiveRecord::Migration
  def change
    create_table :log_events do |t|
      t.string :key

      t.timestamps null: false
    end
  end
end
