class ChangeLogs < ActiveRecord::Migration
  def change
    rename_table :score_logs, :logs
  end
end
