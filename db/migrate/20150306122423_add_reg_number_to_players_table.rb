class AddRegNumberToPlayersTable < ActiveRecord::Migration
  def change
    add_column :players, :reg_number, :integer
  end
end
