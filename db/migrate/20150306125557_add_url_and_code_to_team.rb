class AddUrlAndCodeToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :web, :string
    add_column :teams, :code, :string
  end
end
