class CreateTeamParticipations < ActiveRecord::Migration
  def change
    create_table :team_participations do |t|
      t.integer :team_id
      t.integer :game_id

      t.timestamps null: false
    end
  end
end
