class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :street
      t.string :number
      t.string :city
      t.string :zip

      t.timestamps null: false
    end
  end
end
