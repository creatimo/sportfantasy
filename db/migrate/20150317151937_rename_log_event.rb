class RenameLogEvent < ActiveRecord::Migration
  def change
    rename_column :logs, :event_id, :event
  end
end
