class CreateUserPlayerDeals < ActiveRecord::Migration
  def change
    create_table :user_player_deals do |t|
      t.integer :userteam_id
      t.integer :player_id

      t.timestamps null: false
    end
  end
end
