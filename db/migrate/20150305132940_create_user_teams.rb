class CreateUserTeams < ActiveRecord::Migration
  def change
    create_table :user_teams do |t|
      t.string :name
      t.integer :game_id

      t.timestamps null: false
    end
  end
end
