class ExtendPlayers < ActiveRecord::Migration
  def change
    add_column :players, :birth, :datetime
    add_column :players, :reg_date, :date
  end
end
