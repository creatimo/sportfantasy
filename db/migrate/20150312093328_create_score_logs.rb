class CreateScoreLogs < ActiveRecord::Migration
  def change
    create_table :score_logs do |t|
      t.integer :user_id
      t.datetime :datetime
      t.integer :event_id
      t.integer :event_key
      t.integer :event_value

      t.timestamps null: false
    end
  end
end
