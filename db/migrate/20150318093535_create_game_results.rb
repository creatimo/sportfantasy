class CreateGameResults < ActiveRecord::Migration
  def change
    create_table :game_results do |t|
      t.integer :game_id
      t.text :results
      t.integer :status
      t.datetime :updated_at
    end
  end
end
