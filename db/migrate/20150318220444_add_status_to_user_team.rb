class AddStatusToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :status, :integer
  end
end
