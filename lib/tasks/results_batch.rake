require 'rubygems'
require 'nokogiri'
require 'open-uri'

namespace :results do

  desc "Import Results"
  url = 'http://hokejbal.cz/'

  task :batch => :environment do

    match_id = 143696
    results_url = "#{url}detail-utkani/#{match_id}/"
    page = Nokogiri::HTML( open(results_url) )

    raise 'Results title: ' + page.css('title').text

    mathResult = page.css('.round .match-result')

    mathResult.each do |match|
      raise match.inspect
      score = match.css('.match-score').text.split(' — ')
      score_home = score[0]
      score_away = score[1]
      matchUrl = url + match.next_element.css('tbody').css('tr')[2].css('a')[0]["href"]

      puts 'Opening detail: ' + matchUrl
      matchDetail = Nokogiri::HTML( open(matchUrl) )

      goals = matchDetail.css('.round > table')[1].css('> tbody > tr > td')[0]
      goals_away = goals.css('td')
      puts goals_away if goals_away

      puts '-------------------------'
      puts 'Home team: ' + match.css('.team-home').text + ' | ' + score_home
      puts 'Away team: ' + match.css('.team-away').text + ' | ' + score_away
      puts 'Link: ' + match.next_element.css('tbody').css('tr')[2].css('a')[0]["href"]
      #puts 'Goals: ' + goals_away.text if goals_away
    end

  end

end
