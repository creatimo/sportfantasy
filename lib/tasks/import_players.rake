require 'csv'

namespace :import do

  desc "Import CSV Players"
  task :players => :environment do

    csv_file_path = 'db/import/players.csv'

    CSV.foreach(csv_file_path) do |row|
      team = Team.select(:id, :code, :name).where(code: row[1]).first
      Player.create!({
        :name => row[2],
        :team_id => team.id,
        :reg_number => row[0],
        :reg_date => row[4],
        :birth => row[3]
      })
      puts row[2] + ' [' + team.name + '] added!'
    end
  end
end
