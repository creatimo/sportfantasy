require 'rubygems'

require "#{Rails.root}/app/helpers/logs_helper"
include LogsHelper

namespace :results do

  task :process => :environment do
    load_user_scoring_definition
    @status = true

    test_results.each do |team_results|
      team_process(team_results)
    end

    puts @notice.to_s unless @status
  end
end

private

  def sport_definition
    'hokejbal'
  end

  def load_user_scoring_definition
    sport_key = sport_definition
    @scoring = Settings.sports[sport_key].user_scoring
  end

  def team_process(team_results)
    team_results[:players].each do |player_results|
      player_process(player_results)
    end
  end

  def player_process(player_results)
    player = recognize_player(player_results[:name])
    if player
      users_with_player_in_team(player).each do |user|
        process_user_score(user[:user], player, player_results[:score], user[:captain])
      end
    end
  end

  def process_user_score(user, player, player_results, captain)
    return unless user
    score_update = 0
    player_results.each do |score_key, score_val|
      actual_score_update = @scoring.player[score_key] * score_val
      score_update += actual_score_update
    end
    user.increment(:score, score_update)
    if player.save
      create_log(user.id , :user_score_update_player, player.id, score_update)
    end
  end

  def users_with_player_in_team (player)
    users_list = [] unless users_list
    active_user_teams(player).each do |user_team|
      user = user_team.user
      captain = player.user_player_deals.find_by(user_team_id: user_team.id).captain
      users_list << {user: user, captain: captain}
    end
    users_list.uniq
  end

  def active_user_teams (player)
    player.user_teams.select {|team| team["status"] == 0}
  end

  def recognize_player(name)
    players = Player.where('name like ?', "%#{name}%")
    player = players.first if players.count == 1

    if player
      player
    else
      @status = false
      @notice = 'unrecognized player: ' + name.to_s
      return
    end
  end


  def test_results
    [
      {
        name: 'tým A',
        score: 2,
        players: [
          {
            name: 'Jméno hráče 1',
            score: {
              goal: 1,
              faul: 2
            }
          },
          {
            name: 'Pospíšil Jan',
            score: {
              faul: 1
            }
          },
          {
            name: 'Dědič Michal',
            score: {
              goal: 4
            }
          }
        ]
      },
      {
        name: 'tým B',
        score: 2,
        players: [
          {
            name: 'Hnízdil Michal',
            score: {
              goal: 3,
              faul: 2
            }
          },
          {
            name: 'Jméno hráče 4',
            score: {
              goal: 2
            }
          }
        ]
      }
    ]
  end
