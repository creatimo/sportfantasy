require 'csv'

namespace :import do

  desc "Import CSV Games"
  task :games => :environment do

    csv_file_path = 'db/import/games.csv'

    CSV.foreach(csv_file_path) do |row|
      Game.create!({
        :league_id => 1,
        :term => row[0]
      })
      puts row[0] + " added!"
    end
  end
end
