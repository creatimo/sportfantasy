require 'csv'

namespace :import do

  desc "Import CSV Teams"
  task :teams => :environment do

    csv_file_path = 'db/import/teams.csv'

    CSV.foreach(csv_file_path) do |row|
      Team.create!({
        :name => row[0],
        :league_id => 1,
        :web => row[2],
        :code => row[1]
      })
      puts row[0] + " added!"
    end
  end
end
