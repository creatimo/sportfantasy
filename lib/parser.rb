require 'rubygems'
require 'nokogiri'
require 'open-uri'

module Parser

  PARSER = YAML.load_file("#{Rails.root}/config/settings/parsers.yml")

  def self.parse (game)
    @parser = PARSER[game.parser]
    results_url = @parser['url'] + game.results_source
    page = self.grab_url (results_url)
    parsed_results = self.build_results(page)
    game.parsed_results = parsed_results
    game.status = :parsed
    if game.save
      raise game.parsed_results
    end
  end

  def self.build_results (page)
    results = []
    self.search_teams(page).each_with_index do |team, index|
      team = {
        name: team,
        players: self.build_players(page, index)
      }
      results << team
    end
    results
  end

  def self.build_players (page, index)
    players = []
    self.search_players(page, index).each do |player|
      players << {
        name: player,
        score: {
          goals: self.search_goals(page, player)
        }
      }
    end
    players
  end

  def self.grab_url (url)
    Nokogiri::HTML( open(url) )
  end

  def self.page_title (page)
    path = @parser['title']
    page.css(path).text
  end

  def self.match_detail (page)
    path = @parser['match_detail']['path']
    page.css(path)
  end

  def self.search_players (page, index)
    players = []
    players_table = page.at('h3:contains("Sestavy týmů")').next_element
    first_player_elm = players_table.css('tr td.w15p.text-center').first
    closest_tr = first_player_elm.ancestors('tr').first.ancestors('tr')
    team_players = closest_tr.css('> td')[index]
    team_players.css('td.w15p.text-center').each do |player|
      if player.next_element
        player_name = player.next_element.text
        player_name.slice! "(C)"
        players << player_name.strip
      end
    end
    players
  end

  def self.search_teams (page)
    teams_wrapper = self.search_teams_wrapper (page)
    teams = [] if teams == nil
    teams_wrapper.css('th.w50p').each do |team_wrapper|
      teams << team_wrapper.text
    end
    teams
  end

  def self.search_teams_wrapper (page)
    page.at('h3:contains("Sestavy týmů")').next_element
  end

  def self.search_goals (page, player)
    goals_table = self.search_goals_wrapper (page)
    player_locate = goals_table.at('td b:contains("'+player+'")')
    if player_locate
      player_locate.count.to_i + 1
    else
      0
    end
  end

  def self.search_goals_wrapper (page)
    page.at('h3:contains("Evidence branek")').next_element
  end
end
